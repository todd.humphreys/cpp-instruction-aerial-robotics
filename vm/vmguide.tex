\documentclass[11pt]{article}
\usepackage{subfigure,wrapfig,graphicx,booktabs,fancyhdr,amsmath,amsfonts}
\usepackage{bm,amssymb,amsmath,amsthm,wasysym,color,fullpage,setspace,multirow}
\usepackage[colorlinks=true, linkcolor=blue, citecolor=blue, urlcolor=blue]{hyperref}
\newcommand{\vb}{\boldsymbol}
\newcommand{\vbh}[1]{\hat{\boldsymbol{#1}}}
\newcommand{\vbb}[1]{\bar{\boldsymbol{#1}}}
\newcommand{\vbt}[1]{\tilde{\boldsymbol{#1}}}
\newcommand{\vbs}[1]{{\boldsymbol{#1}}^*}
\newcommand{\vbd}[1]{\dot{{\boldsymbol{#1}}}}
\newcommand{\vbdd}[1]{\ddot{{\boldsymbol{#1}}}}
\newcommand{\by}{\times}
\newcommand{\tr}{{\rm tr}}
\newcommand{\cpe}[1]{\left[{#1} \times \right]}
\newcommand{\sfrac}[2]{\textstyle \frac{#1}{#2}}
\newcommand{\ba}{\begin{array}}
\newcommand{\ea}{\end{array}}
\renewcommand{\earth}{\oplus}
\newcommand{\sinc}{{\rm \hspace{0.5mm} sinc}}
\newcommand{\tf}{\tilde{f}}
\newcommand{\tbox}[1]{\noindent \fbox{\parbox{\textwidth}{#1}}}
\DeclareMathAlphabet{\mathpzc}{OT1}{pzc}{m}{it}

\title{Introduction to Virtual Machines, Linux, and Git}

\begin{document}
%\onehalfspace
\maketitle

\section{Virtual Machine Setup}
In the latter half of the Aerial Robotics course, you will develop inside a
virtual machine (VM). The VM will emulate Ubuntu, a free, popular operating
system built around the Linux kernel.  We will run our VM on an application made
by VMWare (now owned by Broadcom).

\subsection{Requirements}
\begin{enumerate}
\item VMWare virtualization software
\item 30 GB free storage on your computer
\item 10 GB or more of memory on your computer.  (See Section
  \ref{sec:run-virtual-machine} below if your computer doesn't meet this
  requirement.)
\end{enumerate}

\subsection{Install VMWare}
VMware has different virtualization packages for Windows, Linux, and Mac. Follow
the steps below to download and install the free VM software appropriate to your
operating system.

\begin{enumerate}
\item Use your {\bf UT email} to \href{https://profile.broadcom.com/web/registration}{create a Broadcom account}. On the ``build my profile'' page click ``I'll do it later.''
\item On the \href{https://www.vmware.com/products/desktop-hypervisor/workstation-and-fusion}{VMWare Hypervisor product page} click download and sign in with your account information.
\item Navigate to the ``VMware Cloud Foundation'' product site. From here download ``VMWare Workstation Pro'' application if using Windows/Linux or download the ``VMWare Fusion Pro'' application if using MacOS.
\end{enumerate}

The latest version of the software should work on most operating
systems\footnote{Note that if Ubuntu is your operating system, you can run
  everything natively---no need for a VM.  But you'll be responsible for
  installing ROS, OpenCV, and other required libraries, and for changing file
  paths as necessary.}.

\subsection{Download the Virtual Machine}
A customized VM for the Aerial Robotics class has been uploaded to the
Radionavigation Lab's website here for x86 platforms:
{\footnotesize
\begin{verbatim}
https://rnl-data.ae.utexas.edu/datastore/aerialRobotics/Aerial%20Robotics%20x86.vmwarevm.zip
\end{verbatim}
} If your laptop is built on the ARM architecture (Macs with M-series chips fall
into this category), use this VM instead: {\footnotesize
\begin{verbatim}
https://rnl-data.ae.utexas.edu/datastore/aerialRobotics/Aerial%20Robotics%20Arm.vmwarevm.zip
\end{verbatim}
}

Download the VM when you're connected directly to the campus network---it's a
large file!  After downloading, extract the zipped folder (you'll need about 30
GB of free storage) and move the folder to a convenient location on your machine.

\vspace{0.8mm} \textbf{If your host operating system is Linux and {\tt unzip} does
  not work for you. Try using {\tt 7z} instead:}
\begin{verbatim}
sudo apt install p7zip-full
7z e <file-to-extract> -o <output-directory>
\end{verbatim}

\subsection{Run the Virtual Machine}
\label{sec:run-virtual-machine}
\begin{enumerate}
\item If you have the VMWare software properly installed, you can just run the
  virtualizer application (VMWare Workstation Pro or VMWare Fusion Pro), then
  open the VM by File $\rightarrow$ Open and direct the dialog box to where you
  stored the VM. If VMWare asks you how you got the VM, click ``I copied it.''

\item If you don't have at least 10 GB of memory on your computer, you'll need to
  reduce the amount of memory allocated to the VM.  In the virtualizer
  application, go to Virtual Machine Settings $\rightarrow$ Memory and reduce the
  allocated memory.  Any amount above 4 GB should be adequate.

\item Load the VM by selecting File $\rightarrow$ Open a Virtual Machine and browsing
  to the virtual machine configuration (.vmx) file.  Once this is done, run the VM
  by selecting the virtual machine and then selecting Virtual Machine
  $\rightarrow$ Power $\rightarrow$ Play Virtual Machine.  The first boot may take
  several minutes.

\item At the login screen, select the user {\tt aeronaut}. The password is
  {\tt aerial}.  You may need to select the ``Exit full screen mode'' button on
  the VMware bar and then select it again (as ``Enter full screen mode'') to get a
  full-sized display.

\item You can customize your background by right clicking and selecting ``Change
  Background ...''  You can power down the VM by clicking on the power button in
  the top-right corner.
\end{enumerate}

{\bf Please do all your work in the {\tt aeronaut} account.  Don't create
another account or rename {\tt aeronaut}; this will cause trouble with path
names and shared libraries.}

\subsection{Troubleshooting}

\subsubsection{Unable to load the VM on Mac}
\begin{enumerate}
	\item Shut down the Virtual Machine.
	\item Quit VMWare Fusion Pro.
	\item Click on the Apple logo on the top left of the Mac
	      screen$\ \to\ $\textit{Security and Privacy}$\ \to\ $\textit{General}.
	      If there is a message reading ``System Software from VMWare is being blocked,''
	      click \textit{Allow}.
	\item Relaunch VMWare Fusion Pro.
	\item Restart the Virtual Machine.
\end{enumerate}
\subsubsection{VM boot sequence stuck on ``SMBus host controller not enabled''}
These instructions apply to Windows users:
\begin{enumerate}
\item Hold Shift while VM is booting
\item Select Recovery mode
\item Select root to open root shell
\item Run the following commands:
  \subitem mount -o rw,remount /
  \subitem sudo nano /etc/modprobe.d/blacklist.conf
\item Add the folowing line at the bottom of the config file: blacklist i2c-piix4
\item Run the following commands:
  \subitem update-initramfs -u
  \subitem reboot
\end{enumerate}

\subsubsection{VM boot sequence takes too long}
The first time you boot up the VM, it may take several minutes.  Thereafter, a
normal boot time is less than one minute.  If it takes longer than this, you can
kill the VMWare utility (e.g., using Task Manager in Windows), then try the
following.
\begin{enumerate}
\item Make sure your computer has an Internet connection.
\item The VM may not have been shut down cleanly on its last use.  Find the
  folder {\tt Aerial Robotics Ubuntu 20.04.vmwarevm} where the VM is stored.
  Enter this folder and remove any files with the extension {\tt lck} or {\tt
    gz}. 
\end{enumerate}

\subsection{Initial Setup}
Once you're logged in to the {\tt aeronaut} account on the Ubuntu VM, perform
the following steps to complete your initial setup.
\begin{enumerate}
\item Open a terminal by typing {\tt Ctrl-Alt-T}.
\item Change your password: In the terminal type {\tt passwd} and follow the
  instructions.
\item Check that you have internet access:  In the terminal, type
\begin{verbatim}
ping 8.8.8.8
\end{verbatim}
  If you see a response similar to
\begin{verbatim}
64 bytes from 8.8.8.8: icmp_seq=3 ttl=118 time=7.19 ms
\end{verbatim}
  then you have Internet access.
\item If you do not have Internet access, go to \textit{VM} $\rightarrow$
  \textit{Settings}  $\rightarrow$ \textit{Network Adapter} and Select NAT.  If you
  still don't have access, contact the TA.
\item Update the applications and OS software in your VM:
\begin{verbatim}
sudo apt update 
sudo apt upgrade
sudo apt autoremove
omz update
\end{verbatim}
\item Update the configuration files that we use for tmux, vim, etc.:
\begin{verbatim}
cd ~/dotfiles
git pull 
\end{verbatim}
\end{enumerate}

\section{Introduction to C++}
The TA will run a series of recitation sessions to introduce you to programming
in C++ in the Linux environment. The sessions will draw on the materials found
\href{https://gitlab.com/todd.humphreys/cpp-instruction-aerial-robotics/-/tree/master/cpp/basic?ref_type=heads}{here}.

\section{Linux Basics}
\begin{itemize}
	\item The root of the file system (think \verb|C://|) is ``\verb|/|''
	\item Your home directory is \verb|/home/aeronaut/|
	\item The shortcut for your home directory is ``\verb|~|''
	\item The shortcut for the current directory is ``\verb|.|''
	\item The shortcut for the current directory's parent directory is ``\verb|..|''
	\item Files and folders are case-sensitive.
	\item Spaces in directory and file names must be quoted or escaped. For
	      example, for a directory named \verb|Aerial Robotics|, the command line
	      designation is \verb|Aerial\ Robotics|.  It's best to avoid spaces in
	      directory and file names; e.g., make the directory name
	      \verb|aerial-robotics| instead.
\end{itemize}

\subsection{Basic Commands}
\begin{itemize}
	\item \verb|man|: The \verb|man|ual for any command.
	\item CTRL-c: Kills the current process.
	\item \verb|ls|: Lists the files in the current directory.
	\item \verb|mkdir|: Creates a directory.
	\item \verb|rmdir|: Removes a directory.
	\item \verb|touch|: Creates a file.
	\item \verb|pwd|: Prints the current working directory.
	\item \verb|rm|: Removes files (and directories with the \verb|-r| option). Be careful!
	\item \verb|cp|: Copies from source to destination.
	\item \verb|mv|: Moves from source to destination (also used to rename).
	\item \verb|cat|: Outputs contents of file to command line.
\end{itemize}

\section{Git}
\subsection{What is Git?}
Git is a version control system, originally created and used for Linux kernel
development. Git is now by far the most popular version control system. You
will need to use git for this course's final project, and you may find it
useful for other tasks like tracking your \LaTeX~source files. There are many
git commands and options, but you will only need a few of them.

\subsection{Basic Git Commands}
\begin{itemize}
	\item \verb|git init|: Initializes a new git repository in the working directory.
	\item \verb|git clone <server>|: Clones a remote repository to the working directory.
	\item \verb|git add <filename>|: ``Stages'' \verb|<filename>| for commit.
	\item \verb|git commit -m "Commit message"|: Commits staged files.
	\item \verb|git remote add origin <server>|: Adds \verb|<server>| as ``remote'' repository ``origin'' (default).
	\item \verb|git push origin master|: Pushes local commits to the ``master'' (default) branch of the remote repository ``origin''.
	\item \verb|git checkout -b <branchname>|: Creates and switches to new branch \verb|<branchname>|.
	\item \verb|git push origin <branchname>|: Push branch \verb|<branchname>| to the remote repository ``origin''.
	\item \verb|git checkout master|: Switch back to the ``master'' branch.
	\item \verb|git pull|: Updates your local repository to the newest commit on the remote repository.
	\item \verb|git merge <branchname>|: Merges \verb|<branchname>| into the active branch.
	\item \verb|git diff <source_branch> <target_branch>|: Display differences between two branches.
	\item \verb|git log --graph  --abbrev-commit --decorate --all|: Displays branches and commits command window.
\end{itemize}

\subsubsection{Merge Conflicts}
Hopefully, you won't run into these. However, when you have multiple people
editing the same file, there is a good chance that you will run into a
\emph{merge conflict}. Git does a pretty good job of automatically merging
files, but sometimes you will have to do it manually. Git will mark the
conflicts by adding lines to your source files. Once you have resolved the
conflict and deleted the Git-added lines by manually editing your files, stage
and commit the changes.

\subsection{GitLab}
GitLab is an online hosting service using Git.  If you don't already have one,
create an account on \href{gitlab.com}{GitLab}.

\subsection{Create an SSH Key Pair}
The easiest way to validate your credentials with GitLab so that you can push
to and pull from private respositories on GitLab is to create an SSH key pair.
This consists of a secret private key and a corresponding public key.  Create
the keys as follows in a terminal:
\begin{verbatim}
ssh-keygen -t rsa -b 2048 -C "<comment>"
\end{verbatim}
In response to each of the subsequent queries, just hit Enter.  You can link
your {\tt aeronaut} account on the VM with your GitLab account by copying your
public key onto your GitLab key list.  On the VM, go to the directory where
your key pair resides and print the public key to the terminal:
\begin{verbatim}
cd ~/.ssh
cat id_rsa.pub
\end{verbatim}
Copy the long key that shows up and paste it on your GitLab account:
\begin{enumerate}
	\item Click on the circular icon in the upper right.
	\item Select Preferences.
	\item Select SSH Keys from the menu on the left.
	\item Paste the public key into the box.
\end{enumerate}

\subsection{First-Time Git Setup}
\begin{verbatim}
git config --global user.name "Your Name"
git config --global user.email "your.email@utexas.edu"
git config --global core.editor <your preferred editor>
\end{verbatim}
Your preferred editor could be \verb|code| (for VS Code), \verb|vim|,
\verb|emacs|, \verb|nano|, or some other editor.

\section{Exercises}
\subsection{Unix}
\begin{enumerate}
	\item Using the terminal, switch to the home directory. % cd ~
	\item Create a new directory called ``cpp-exercises'' (or whatever you want) % mkdir cpp-exercises
	\item Switch to the directory you just created. % cd cpp-exercises
	\item Create a new file ``README.md'' in the directory containing the repo title and your name, then save and exit back to the terminal. % sublime/nano/gedit/vim/emacs
\end{enumerate}

\subsection{Git}
In the directory you created for the Unix exercises,
\begin{enumerate}
	\item Initialize a git repository in the directory. % git init
	\item Stage the README you created for commit. % git add -A or git add README.md
	\item Commit the README to the repo with a message of choice. % git commit -m "message"
\end{enumerate}
\textit{Note}: you will need to set a username and email address before committing; for simplicity, make it the same as the one you use for Gitlab (see below). This tells other users who to ``blame'' for specific changes in the code base.

\subsection{GitLab}
\begin{enumerate}
	\item Create a new empty project for the git repo you created on the VM.
	\item Following the instructions for an existing Git repo on the Gitlab page, add the new remote origin.
	\item Push your local commits to the remote repository.
	\item Edit the README on the Gitlab web page and save the changes.
	\item From the terminal, pull the changes from the remote to the local repository.
	\item Output the contents of the README to the terminal.
\end{enumerate}
You might find it easier to view some of your files on Gitlab than in the
terminal. In Gitlab, mouse over the ``Repositories'' tab on the left-hand side,
and check out the ``Commits'', ``Branches'', ``Graph'', and ''Compare'' pages.

\section{Cloning the Game Engine}
You will implement Lab 4, Lab 5, and the final project on the VM. Instead of
downloading MATLAB ``temp" files from Canvas, you will get all the code you need
from Gitlab. For Lab 4, you will need to clone the \verb|game-engine|
repository. Follow the instructions in this
\href{https://gitlab.com/radionavlab/public/game-engine/-/blob/master/README.md}{README}
to clone and build \verb|game-engine|. Please reach out to the TA if you have
trouble.

\section{Frequently Asked Questions}
\subsection*{What is a Virtual Machine?}
A virtual machine (VM) is an emulation of another computer within your
computer. With a VM, you can emulate Mac on Windows, or Linux on Mac, etc. VMs
let you develop software for another machine without the need to change your
computer.

\subsection*{Why are we using a VM?}
It's difficult to provide course materials and debug problems across multiple
different types of machines and operating systems. By using the provided VM,
everyone's development platform is guaranteed to be identical and therefore
problems/solutions will be identical as well.

\subsection*{Why are we using Ubuntu Linux?}
ROS, a core component of the software that will be used in this course, works
exclusively on Ubuntu Linux.

\subsection*{Can I use other virtual machine software packages like VirtualBox?}
No. We have tried other VM software, but have always encountered a problem down
the road. As far as we know, the only VM software that has worked flawlessly is
VMWare.

\subsection*{Can I develop the code on Windows/Mac without the VM?}
No. The software depends on ROS, which only runs on Ubuntu.

\section{Questions and Concerns}
If you have any questions or concerns, send an email to the TA. Alternatively,
you may google your question and find a suitable answer on StackOverflow, or ask
Copilot (free for UT students), ChatGPT, etc.

\section{References}
\begin{itemize}
\item Helpful introductory videos from MIT's ``Missing Semester'':
  \begin{itemize}
  \item Course overview + the shell: \href{https://missing.csail.mit.edu/2020/course-shell/}{link}
  \item Version Control (Git):
    \href{https://missing.csail.mit.edu/2020/version-control/}{link}
  \item Command-line Environment (including terminal multiplexer {\tt tmux}): \href{https://missing.csail.mit.edu/2020/command-line/}{link}
  \end{itemize}
\item Git: The Simple Guide: \href{https://rogerdudler.github.io/git-guide/}{link}
\end{itemize}

\end{document}
