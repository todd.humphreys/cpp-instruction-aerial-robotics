#include <iostream>
#include <vector>
#include <string>

using namespace std;

// https://www.geeksforgeeks.org/program-for-nth-fibonacci-number/
vector<int> fib(int n) 
{ 
  /* Declare an array to store Fibonacci numbers. */
  vector<int> f;   // 1 extra to handle case, n = 0 
  size_t i; 
  
  /* 0th and 1st number of the series are 0 and 1*/
  f.push_back(0);
  f.push_back(1);
  
  for (i = 2; i <= n; i++) 
  { 
      /* Add the previous 2 numbers in the series 
         and store it */
      f.push_back(f[i-1] + f[i-2]); 
  }
  
  return f;
}

int main(int argc, char** argv){
    //int n = 9; // old way

    int n;

    string arg = argv[1];
    try {
        size_t pos;
        n = stoi(arg, &pos);
        if (pos < arg.size()) {
            cerr << "Trailing characters after number: " << arg << '\n';
        }
    } catch (std::invalid_argument const &ex) {
        cerr << "Invalid number: " << arg << '\n';
        return -1;
    } catch (std::out_of_range const &ex) {
        cerr << "Number out of range: " << arg << '\n';
        return -2;
    }

    vector<int> a = fib(n);

    for (size_t x : a) 
        cout << x << " ";
    cout << "\n";

    return 0;
}