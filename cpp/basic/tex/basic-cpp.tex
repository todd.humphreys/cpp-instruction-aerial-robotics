\documentclass[11pt]{article}
\usepackage{subfigure,wrapfig,graphicx,booktabs,fancyhdr,amsmath,amsfonts}
\usepackage{bm,amssymb,amsmath,amsthm,wasysym,color,fullpage,setspace,multirow}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{float}
\newcommand{\vb}{\boldsymbol}
\newcommand{\vbh}[1]{\hat{\boldsymbol{#1}}}
\newcommand{\vbb}[1]{\bar{\boldsymbol{#1}}}
\newcommand{\vbt}[1]{\tilde{\boldsymbol{#1}}}
\newcommand{\vbs}[1]{{\boldsymbol{#1}}^*}
\newcommand{\vbd}[1]{\dot{{\boldsymbol{#1}}}}
\newcommand{\vbdd}[1]{\ddot{{\boldsymbol{#1}}}}
\newcommand{\by}{\times}
\newcommand{\tr}{{\rm tr}}
\newcommand{\cpe}[1]{\left[{#1} \times \right]}
\newcommand{\sfrac}[2]{\textstyle \frac{#1}{#2}}
\newcommand{\ba}{\begin{array}}
\newcommand{\ea}{\end{array}}
\renewcommand{\earth}{\oplus}
\newcommand{\sinc}{{\rm \hspace{0.5mm} sinc}}
\newcommand{\tf}{\tilde{f}}
\newcommand{\tbox}[1]{\noindent \fbox{\parbox{\textwidth}{#1}}}
\DeclareMathAlphabet{\mathpzc}{OT1}{pzc}{m}{it}

\newtheorem{exercise}{Exercise}[section]

\title{Introduction to C++\\for Aerial Robotics}
\date{\today}

\begin{document}
%\onehalfspace
\maketitle

\section{Introduction}
C++ is one of the most widely-used programming languages in the world; it
evolved from C and includes many more advanced concepts, especially for
object-oriented programming. It is especially useful for systems programming
(operating systems) and embedded programming (microcontrollers). C++ is
typically much faster than MATLAB, Java, or Python, but can be a little more
difficult to learn.

\section{Hello World}
Open up a terminal on your VM.
\subsection{\#Includes}
% https://www.learncpp.com/cpp-tutorial/introduction-to-the-preprocessor/
% why do you need to tell the C++ compiler what external libraries to look for?
When the compiler sees an \verb|#include|, it replaces the \verb|#include|
directive with the contents of the \verb|#include|d file. You'll mainly see
\verb|#include|s and other preprocessor directives (at the beginning of the
file, preceded by a hash \#) in \emph{header files}. Header files are used to
include libraries and define functions for the compiler. Although it's
possible to have simple C++ programs without header files, it's good practice
for larger projects to use them. Each C++ (\verb|.cc| or \verb|.cpp|) source
file will have a corresponding \verb|.h| header file.

%\begin{exercise}
%What \verb|#include| would we need to display text to the command line?
% answer: #include <iostream>
%\end{exercise}

\subsection{Functions}
Functions must be declared with return types, a name, and a pair of
parentheses containing the arguments. The body of the function is enclosed
within curly braces. The \verb|main| function is a special function that is
called when the program is executed.

The \verb|main| function must always return an \verb|int|eger. It may have no
arguments (\verb|()| or \verb|(void)|) if it is not going to process command
line arguments. If it does need to process command line arguments, it should
be defined as \verb|int main(int argc, char** argv)|. \verb|argc| is the
\emph{argument count}, while \verb|argv| is the argument vector. More on what
that means later.

\subsection{I/O}
% how to (1) get C++ to print out text
% and    (2) input text/numbers to C++

In a new file \verb|helloworld.cc|, write the following:
\begin{lstlisting}[language=C++]
#include <iostream> // standard library for text I/O

// <- end-of-line comment

/* <- multiline comment
    Function hello_world()
*/
void hello_world(){
    std::cout << "Hello World!\n";
}

int main(int argc, char** argv){
    hello_world();
    return 0;
}
\end{lstlisting}

\subsection{Compiling Your Code}
% "bare" g++ command
% command to get assembly
% view assembly
% view symbols with nm -an helloworld
% view binary with xxd helloworld
% run executable
% add/commit/push to repo
% commit often! You should make a commit after basically every nontrivial change to your code. That way, if you break something, then you won't have to roll back too far. Also makes resolving merge conflicts easier.

You've written the source code---but how do you get it to run? C++ is a
compiled language, meaning that the source code has to be compiled into a
separate executable file which is the thing you actually run. We'll use
\verb|g++|, the GNU compiler for C++.

Run
\begin{lstlisting}[language=bash]
$ g++ -std=c++11 -o helloworld helloworld.cc
\end{lstlisting}
The option \verb|-std=c++11| ensures that the code is compiled according to
the C++ 11 standard (not that it really matters for this example). Run the
resulting binary (executable), \verb|helloworld|.
\begin{lstlisting}[language=bash]
$ ./helloworld
\end{lstlisting}

What's actually going on under the hood? Let's inspect the assembly code and
the binary to find out.
\begin{lstlisting}[language=bash]
$ g++ -std=c++11 -S -fverbose-asm helloworld.cc
$ g++ -std=c++11 -o helloworld helloworld.cc
\end{lstlisting}
The compiler translates the \verb|.cc| source file into the assembly code
contained in \verb|helloworld.s|.
\begin{lstlisting}[language=bash]
$ cat helloworld.s
\end{lstlisting}
This assembly code is a (barely) human-readable form of the machine code that
the CPU executes. You may recognize some of the symbols, such as the
\verb|.string| that you are printing and the (mangled) function names. Note
that assembly code is specific to the processor, and is translated directly in
to the binary ``machine code'' that the processor executes.

These same symbols can be seen after running the GNU binary utility \verb|nm|
on the binary.
\begin{lstlisting}[language=bash]
$ nm -an helloworld
\end{lstlisting}

The actual machine code is even uglier.
\begin{lstlisting}[language=bash]
$ cat helloworld
\end{lstlisting}
However, it becomes a little more readable if you look at the binary code in
hexadecimal form:
\begin{lstlisting}[language=bash]
$ xxd helloworld
\end{lstlisting}
Recall that a single byte has values ranging from 00 to FF in hexadecimal, so
each 4-character cluster is 16 bits. The first column of the output is the
address or position of the data in the file, and the last column is the text
representation of the bytes that have a printable ASCII value.

Note: this binary actually includes much more than just the machine code that
the CPU sees---it is in a container format called Executable and Linkable
Format (ELF).

You won't need to worry about what's going on behind the scenes unless
something goes terribly wrong, but it's helpful to see how close C++ is to the
``bare metal'' compared to MATLAB or Python.

\subsubsection{Makefile}
To simplify the build process, we'll put our compiler shell commands into a
\emph{Makefile}. Create a new file in the same directory called
\verb|Makefile|.
\begin{lstlisting}[language=make]
all: 
    g++ -std=c++11 -S -fverbose-asm helloworld.cc
    g++ -std=c++11 -o helloworld helloworld.cc
\end{lstlisting}
``all'' is the default ``target''. Basically, you want to build all of the files that form the entire program. To build the code, simply use the command
\begin{lstlisting}[language=bash]
$ make
\end{lstlisting}

\subsubsection{Git}
Now that you have made some changes to the repository, stage and commit your
changes. It's a good idea to commit often! You should make a commit after
basically every nontrivial change to your code. That way, if you break
something, then you won't have to roll back too far. Also makes resolving
merge conflicts easier.

\section{Types and Data Structures}\label{sec:types}
You may have noticed that C++, unlike MATLAB, requires that we declare a
\emph{type} when initializing a variable. This tells the compiler what kind of
data is allowed to be stored in a particular variable. This may seem annoying,
but it allows the compiler to check that data types are being used in ways
consistent with their definition. Here are some common types you are likely to
encounter:
\begin{itemize}
\item \verb+double+: This is a double-precision floating point (decimal)
  number. In other words, it's the default type in MATLAB and what you're
  probably most familiar with already.
\item \verb+int+: A signed 16-bit integer, with values ranging from $-32768$
  to $32767$.
\item \verb+unsigned int+: An unsigned 16-bit integer, with values ranging
  from $0$ to $65535$.
\item \verb+size_t+: This special type is used to index into
  arrays/vectors/etc.: it's a special kind of \emph{unsigned} integer, which
  means that it is not permitted to have values less than $0$, and it is the
  type that is returned by the \verb+sizeof+ operator.
\item \verb+bool+: Can represent two states: \verb+true+ or \verb+false+.
\item \verb+char+: A one-byte type that can represent a single character like
  \verb+'a'+.
\end{itemize}

% TODO structs and objects (save for later)

\subsection{Pointers}
A pointer is a special kind of variable that holds the \emph{memory address}
of another variable. Pointers ``point to'' the variable whose memory address
they store.
\begin{lstlisting}[language=C++]
var_type *var_name; 
\end{lstlisting}
For example, 
\begin{lstlisting}[language=C++]
int *ptr; //ptr can point to an address which holds int data
\end{lstlisting}
There are two operators associated with pointers: the dereference operator
(\verb+*+) and the address-of operator (\verb+&+).
\begin{itemize}
\item \verb+&+ is the \textbf{address-of} operator, and can be read simply as
  ``address of.''
\item \verb+*+ is the \textbf{dereference} operator, and can be read as
  ``value pointed to by.''
\end{itemize}

Here's an example of how they relate to each other:
\begin{lstlisting}[language=C++]
int n, m;
n = 7;
int *n_addr = &n;
m = *n_addr;
\end{lstlisting}
\verb+n_addr+ (we can call it whatever we want) is a pointer that contains the memory address of the \verb+int+ \verb+n+. \verb+m+ and \verb+n+ should both be equal to 7.

Why are pointers useful? Well, passing the address of an object in memory is often more efficient than passing the object itself. Because directly accessing memory can come with risks, the standard library contains so-called ``smart pointers'' (like \verb+std::shared_pointer+) that have additional features and applications.

\section{Functions and Loops}
\subsection{Functions}
Functions in C++ must declare what type the argument and return variables are.
\begin{lstlisting}[language=C++]
return_type function_name (argument1_type argument1_name, ...) { 
    return_type x;
    // do stuff here
    return x;
}
\end{lstlisting}
In this example, the \verb+return_type+ could be any of the types presented in Section \ref{sec:types} (or others). Unlike MATLAB, we need to tell the function to return our example variable \verb+x+. In addition, we are only permitted to return one variable! If you want to return more than one variable, you'll have to consider packaging them in a \verb+std::vector+, \verb+std::pair+, \verb+struct+, or \verb+class+.

Functions in C++ can also pass variables by reference as shown below. 
\begin{lstlisting}[language=C++]
return_type function_name (argument1_type& argument1_name, ...) { 
    // do stuff here
}
\end{lstlisting}
In this example the function argument is passed by reference. This is achieved using the \textbf{address-of} operator \verb+&+. This causes a reference to the variable to be passed into the function. There are two reasons to pass a variable by reference. For large data types such as a large object it will be faster to pass by reference. Passing by reference also allows the function to change the value of the variable passed to the function.  

Every executable in C++ calls a function \verb+main()+, with the following format:
\begin{lstlisting}[language=C++]
int main(int argc, char** argv) {
    // do stuff here
    return 0;
}
\end{lstlisting}
\begin{itemize}
\item The \verb+main()+ function always returns an \verb+int+, which is the exit code for the process ($0$ if the program exits without error).
\item \verb+main()+ can be called without arguments, as \verb+int main()+, but otherwise it has two arguments: \verb+int argc+ and \verb+char** argv+. 
    \begin{itemize}
    \item \verb+argc+ is the argument count for command line arguments contained in \verb+argv+. \verb+argc+ is always $\geq 1$, because \verb+argv[0]+ is the name of the program.
    \item \verb+argv+ is an array of character pointers corresponding to the command line arguments.
    \end{itemize}
\end{itemize}


\subsection{Loops}
Loops are similar in function to the loops you implement in MATLAB, with some minor syntax differences.

The while loop is virtually identical to one you would find in MATLAB:
\begin{lstlisting}[language=C++]
while (condition) {
    // do something
}
\end{lstlisting}
where \emph{condition} is a statement that evaluates to a \verb+bool+, e.g. true or false.

For loops are a little different.
\begin{lstlisting}[language=C++]
for (int n = 0; n < 10; n++) {
    // do something
}
\end{lstlisting}
We declare a counter variable and set it equal to zero; at each iteration, the condition \verb+n < 10+ is checked, then the \verb+//do something+ is executed, and finally the counter is incremented \verb+n+++.

There are also more advanced versions of for loops that can be used when looping over a vector, for example, but the syntax presented above can also be used for those cases.

\subsubsection{Increments and Decrements}
In the for loop example, we used an increment operator \verb++++ on \verb+n+. This does essentially the same thing as $n=n+1$. The decrement operator \verb+--+ on \verb+n+ would do essentially the same thing as $n=n-1$. The reason I say \emph{essentially} is that the behavior changes subtly if you use the increment/decrement operators as part of another statement, and whether or not you use the operator in \emph{prefix} (\verb|++n|) versus \emph{postfix} (\verb|n++|) form.

\section{Arrays and Vectors}
An array in C or C++ is literally just the memory address of some adjacent blocks in your computer's memory. An index into an array just skips a number of those blocks (depending on the size of the object stored in the array). If you don't manually check that the index is less than the length of the array, it's possible to accidentally read or write data to parts of your computer's memory that you didn't intend to access. This, as you can imagine, is very bad, and typically causes an error known as a \textit{segfault}.

To remedy this, the C++ standard library has \textit{vectors}: safer and easier-to-use arrays that can be dynamically resized. Like for I/O, we have to tell the preprocessor to look for the standard library component:
\begin{lstlisting}[language=C++]
#include <vector>
\end{lstlisting}
Vectors can be dynamically resized, meaning that the amount of memory allocated to it can change at runtime. However, they are restricted to contain one data type. To initialize a vector of integers, for example, one can use the following two \textit{constructors}:
\begin{lstlisting}[language=C++]
std::vector<int> vec; 
// use initializer list to initialize array
std::vector<int> vec2 = { 9, 7, 5, 3, 1 }; 
\end{lstlisting}
The prefix \verb+std::+ indicates that \verb+vector+ comes from the C++ standard library. If you would like to omit these prefixes, include the line \verb+using namespace std;+ at the beginning of your file.

To add an element to the end of a vector, use \verb+push_back()+:
\begin{lstlisting}[language=C++]
std::vector<int> vec; 
vec.push_back(1); // vec now contains {1}
\end{lstlisting}
To access an element of a vector, use square brackets. Note that C++ indexes begin at $0$, unlike MATLAB.
\begin{lstlisting}
std::vector vec { 1, 2, 3, 4 };
std::cout << vec[2] << "\n"; // prints out 3!
\end{lstlisting}
To iterate over the elements of a vector, you can use the following syntax:
\begin{lstlisting}
std::vector vec { 1, 2, 3, 4 };
for (int i : vec)
    std::cout << i << " ";
 
 std::cout << "\n";
\end{lstlisting}

As you may have noticed, \verb+std::vector+s are lists; they aren't quite like the vectors in MATLAB you can do linear algebra with For actual math, we'll rely on a C++ linear algebra library called \textit{Eigen}---more on this later.

\section{Strings}
Like with \verb+vector+s, you need to \verb+#include+ a standard library to use strings. 
\begin{lstlisting}[language=C++]
#include <string>
\end{lstlisting}
Otherwise, they behave in pretty familiar ways.
\begin{lstlisting}[language=C++]
std::string mystring;
mystring = "This is a string";
std::cout << mystring << std::endl;
\end{lstlisting}

\begin{exercise}
\textbf{The Fibonacci Sequence}. Write a program that takes a positive integer $n$ as input and prints out a vector containing the first $n$ elements of the Fibonacci sequence. Recall that the Fibonacci sequence is defined as follows:
$$F_0 = 0,\ F_1 = 1 $$
$$F_n = F_{n-1} + F_{n-2}$$
\end{exercise}

%\section{Memory and Debugging} % not really important
% valgrind, gdb, etc.

\section{Math and Eigen} % save this for later
Eigen is a linear algebra library for C++. For other projects, you may also find Armadillo (another linear algebra library) useful for its attempt to match MATLAB syntax. More on this later\ldots
% TODO

%\section{Frequently Asked Questions}

\section{Resources}
\begin{itemize}
\item Learncpp.com: \url{https://www.learncpp.com/} (Beginner-level reference)
\item Exercism.io: \url{exercism.io} (Tutorials)
\item C++ Reference: \url{https://en.cppreference.com/w/} (More advanced reference)
%\item Valgrind Quick Start Guide: \url{https://valgrind.org/docs/manual/quick-start.html}
\end{itemize}

\begin{figure}[H]
\centering
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{images/googling-error.jpg}
  \end{minipage}
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{images/stack-overflow.jpg}
  \end{minipage}
  \caption{Additional resources.}
\end{figure}


\section{References}
\begin{itemize}
\item Learncpp.com: \url{https://www.learncpp.com/}
\item Git: The Simple Guide: \url{https://rogerdudler.github.io/git-guide/}
\item CS 3410 Course Files: \url{http://www.cs.cornell.edu/courses/cs3410/2015sp/}
\item Eigen Quick Reference: \url{https://eigen.tuxfamily.org/dox/group__QuickRefPage.html}
\end{itemize}

\end{document}
