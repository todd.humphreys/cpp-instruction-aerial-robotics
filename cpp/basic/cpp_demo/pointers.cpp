#include <iostream>

void change_by_pointer(int *b) {
    std::cout << "before change_by_pointer *b = " << *b << std::endl;
    std::cout << "before change_by_pointer b = " << b << std::endl;
    std::cout << "before change_by_pointer &b = " << &b << std::endl;
    int b_new = 2;
    *b=b_new;
    std::cout << "after change_by_pointer *b = " << *b << std::endl;
}

void change_by_pointer3(int *b) {
    std::cout << "before change_by_pointer *b = " << *b << std::endl;
    std::cout << "before change_by_pointer b = " << b << std::endl;
    std::cout << "before change_by_pointer &b = " << &b << std::endl;
    int b_new = 3;
    *b=b_new;
    std::cout << "after change_by_pointer *b = " << *b << std::endl;
}

void change_by_reference(int &a) {
    std::cout << "a is not a pointer so it cannot be dereferenced with *a" << std::endl;
    std::cout << "before change_by_reference a = " << a << std::endl;
    std::cout << "before change_by_reference &a = " << &a << std::endl;
    int a_new = 1;
    a=a_new;
}

void change(int a) {
    a = 100;
}

void change_by_pointer_no_comments(int *b) {
    int b_new = 3;
    *b=b_new;
}

int main(int argc, char *argv[]) {
    // Initializing an int
    int a=0;
    std::cout << "originally a = " << a << std::endl;
    std::cout << "originally &a = " << &a << std::endl;
    std::cout << "\n" << std::endl;

    // Try to change
    std::cout << "before attempted change a = " << a << std::endl;
    change(a);
    std::cout << "after attempted change a = " << a << std::endl;
    std::cout << "\n" << std::endl;

    // Initializing an int by setting it equal to another variable
    int b=a;

    // Change by reference
    change_by_reference(a);
    std::cout << "after change_by_reference a = " << a << std::endl;
    std::cout << "after change_by_reference &a = " << &a << std::endl;
    std::cout << "b remains unchanged b= " << b << std::endl;
    std::cout << "\n" << std::endl;

    // Change by pointer
    change_by_pointer(&b);
    std::cout << "after change_by_pointer b = " << b << std::endl;
    std::cout << "after change_by_pointer &b = " << &b << std::endl;
    std::cout << "a remains unchanged a= " << a << std::endl;
    std::cout << "\n" << std::endl;

    // Pointer to an object
    int *c= &b;
    std::cout << "b before pointer is passed b= " << b << std::endl;
    change_by_pointer3(c);
    std::cout << "b after pointer is passed b= " << b << std::endl;
    
    return 0;
}
