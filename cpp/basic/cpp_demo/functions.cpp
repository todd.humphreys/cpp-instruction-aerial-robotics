#include <iostream>

int add(int a, int b) {
    return a+b;
}

void printString(char *str) {
    std::cout << str << std::endl;
}

int main(int argc, char *argv[]) {
    int addition;
    addition = add(5,3);
    std::cout << "Add output: " << addition << std::endl;
    printString("Done");
    return 0;
}

