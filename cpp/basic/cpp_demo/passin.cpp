#include <iostream>

int main(int argc, char *argv[]) {
    std::cout << "Passed in " << argc << " of arguments."<< std::endl;
    for (int i=0; i<argc;i++){
        std::cout << "Arguments #" << i << " is: " << argv[i]<< std::endl;
    }
    return 0;
}
