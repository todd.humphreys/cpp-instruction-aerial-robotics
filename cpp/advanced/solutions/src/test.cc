#include "digraph.h"

int main(int argc, char** argv) {
    // Construct three shared_ptr Nodes with payloads "A", "B", and "C".
    std::shared_ptr<Node> n1 = std::make_shared<Node>(Node("A"));
    std::shared_ptr<Node> n2 = std::make_shared<Node>(Node("B"));
    std::shared_ptr<Node> n3 = std::make_shared<Node>(Node("C"));

    // Construct a cost=1 edge between A and B.
    std::shared_ptr<DirectedEdge> d1 = std::make_shared<DirectedEdge>(DirectedEdge(n1, n2, 1));
    // Construct a cost=2 edge between C and B.
    std::shared_ptr<DirectedEdge> d2 = std::make_shared<DirectedEdge>(DirectedEdge(n3, n2, 2));

    // Construct an empty digraph.
    Digraph graph;
    // Add the edges you constructed to the graph.
    graph.edges.push_back(d1);
    graph.edges.push_back(d2);

    // Using NodeVisitor:
    // Initialize an empty stack of shared pointers to NodeVisitor instances.
    std::stack<std::shared_ptr<NodeVisitor>> visitors;
    // Initialize a shared pointer to an (empty) NodeVisitor instance.
    std::shared_ptr<NodeVisitor> nv = std::make_shared<NodeVisitor>();
    // Set the instances parent to nullptr, node to A, and cost to 0.
    nv->parent = nullptr;
    nv->node = n1;
    nv->cost = 0;

    // Push the NodeVisitor instance to the top of the stack.
    visitors.push(nv);

    // begin an infinite loop
    while (true) {
        // Access top of stack, store it in a temporary variable nv_ (of type shared pointer to a NodeVisitor)
        std::shared_ptr<NodeVisitor> nv_ = visitors.top();
        // Print out the temporary NodeVisitor's node's payload.
        std::cout << "Visiting: " << nv_->node->toString() << std::endl;

        // Remove top of stack
        visitors.pop();

        // Get neighboring nodes
        std::vector<std::shared_ptr<Node>> neighbors = graph.Neighbors(nv_->node);

        // End condition: if no neighbors, then break out of the loop.
        if(neighbors.size() == 0) {
            break;
        }

        // Iterate through neighbors
        for(std::shared_ptr<Node> neighbor: neighbors) {
            // Initialize another temporary variable nv__ (of type shared pointer to a NodeVisitor)
            std::shared_ptr<NodeVisitor> nv__ = std::make_shared<NodeVisitor>();
            // Set nv__'s parent to nv_.
            nv__->parent = nv_;
            // Set nv__'s node to the current neighbor. 
            nv__->node = neighbor;
            // increment the cost of nv__
            //nv__->cost = nv_->cost + 1;
            ++(nv__->cost);
            // push nv__ onto the top of the stack
            visitors.push(nv__);
        }
    }

    return EXIT_SUCCESS;
}