#include "digraph.h"

// Node Functions
std::string Node::toString() const {
    return payload;
}

bool Node::operator==(const Node& other) const {
    return payload == other.payload;
}

Node::Node(std::string payload_in) {
    payload = payload_in;
}

// Directed Edge Functions
std::string DirectedEdge::toString() const {
    return 
        source->toString() + " -- " +
        std::to_string(cost) + " -- " +
        sink->toString();
}

DirectedEdge::DirectedEdge(std::shared_ptr<Node> source_in, std::shared_ptr<Node> sink_in, int cost_in){
    source = source_in;
    sink = sink_in;
    cost = cost_in;
}

// Digraph functions
std::vector<std::shared_ptr<Node> > Digraph::Neighbors(const std::shared_ptr<Node> node) const {
    std::vector<std::shared_ptr<Node>> neighbors;
    for (const std::shared_ptr<DirectedEdge>& edge : edges) {
        if(edge->source->payload == node->payload) {
            neighbors.push_back(edge->sink);
        }
    }
    return neighbors;
}

std::string Digraph::toString() const {
    std::string ret_val = "";
        for(const std::shared_ptr<DirectedEdge> edge: edges) {
            ret_val = ret_val + edge->toString() + "\n";
        }
    return ret_val;
}

