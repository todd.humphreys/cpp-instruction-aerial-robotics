#include "digraph.h"

int main(int argc, char** argv) {
    // Construct three shared_ptr Nodes with payloads "A", "B", and "C".

    // Construct a cost=1 edge between A and B.

    // Construct a cost=2 edge between C and B.

    // Construct an empty digraph.

    // Add the edges you constructed to the graph.


    // Using NodeVisitor:
    // Initialize an empty stack of shared pointers to NodeVisitor instances.

    // Initialize a shared pointer to an (empty) NodeVisitor instance.

    // Set the instances parent to nullptr, node to A, and cost to 0.

    // Push the NodeVisitor instance to the top of the stack.

    // (begin an infinite loop)

    // Access top of stack, store it in a temporary variable nv_ (of type shared pointer to a NodeVisitor)

    // Print out the temporary NodeVisitor's node's payload.

    // Remove top of stack

    // Get neighboring nodes

    // End condition: if no neighbors, then break out of the loop.

    // Iterate through neighbors

    // Initialize another temporary variable nv__ (of type shared pointer to a NodeVisitor)

    // Set nv__'s parent to nv_.

    // Set nv__'s node to the current neighbor. 

    // increment the cost of nv__

    // push nv__ onto the top of the stack

	// (end loop)

    return EXIT_SUCCESS;
}
