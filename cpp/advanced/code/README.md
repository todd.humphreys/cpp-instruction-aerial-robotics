# More Advanced C++ Tutorial: Digraph

## Introduction

In this tutorial, you will implement a [directed graph](https://en.wikipedia.org/wiki/Directed_graph) (or digraph).

This is intended to be somewhat free-form, so don't hesitate to look up answers to your questions online (or, ask the TAs).

You may also find it helpful to refer to the VM/Unix/Git and Basic C++ guides posted under labs/lab0 on Canvas.

There will be a similar guide covering more advanced topics relevant to this tutorial posted soon. For now, you may find reviewing the following topics helpful, in addition to the material in the lab0 guides:

* Structs
* Classes
* Smart Pointers
* Math and Eigen
* CMake

## Exercises

Begin by cloning this repository to your virtual machine.

1. Using the comments in ```include/digraph.h``` as a guide, implement the digraph class constructors and methods in ```src/digraph.cc```.
2. Using the comments in ```src/test.cc``` as a guide, build and traverse an instance of the digraph class you implemented.

## Build instructions

Labs 4 and 5, along with the final project, will use the build system CMake.

Here's how you can build your code:
```bash
mkdir build  # create a new directory called build (if building for the first time)
cd build     # navigate to the build directory you just created
cmake ..     # runs cmake to configure the Makefiles
make         # build the code
```

The resulting binaries can then be found in build/src/, or in bin/.